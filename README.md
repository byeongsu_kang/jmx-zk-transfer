# Kafka JMX 데이터와 Zookeeper 데이터를 주기적으로 받아오는 데몬 프로세스입니다.

### Kafka JMX : 작성한 쿼리를 바탕으로 JMX 데이터를 받아옵니다
### Zookeeper : Consumer Group, 새로 생성된 Topic, ISR정보를 받아옵니다.

### Config작성법
meta.properties 에 읽어올 property의 파일명을 입력합니다. 파일간의 구분자는 ','입니다.

ex) jmx.server.list=dev-broker.json,live-broker.json

jmx.server.list의 경우 jmx-servers 디렉토리의 파일을 읽습니다. 클러스터 별로 파일을 분리하여 관리하면 편리합니다.
zookeeper.list의 경우 zookeepers 디렉토리 파일을 읽습니다. 

Zookeeper도 마찬가지로 별도 파일로 분리하여 여러 주키퍼를 관리하면 편리합니다.

jmx-servers의 queries 컬럼을 작성하는 방법은 두 가지가 있습니다.
1. 직접 쿼리를 넣는 방식
2. 중복된 query의 경우 queries디렉토리에 json파일로 생성하고 queries컬럼에 해당 file 명을 넣어주는 방식

중복되는 쿼리를 작성해야 할 때는 파일로 분리하여 입력하는 두 번째 방식이 좋습니다.

### Kafka JMX 쿼리 작성
conf/jmx-servers/queries 에 json형태로 쿼리 작성이 가능합니다.
쿼리 depth가 여러개여도 '*' 을 이용해서 받아올 수 있습니다.

ex) "kafka.server:type=BrokerTopicMetrics,name=MessagesInPerSec,topic=*",
     
위와 같이 *로 처리된 부분은 내부적으로 파씽하여 모든 데이터를 가져옵니다.

### 입수 주기
현재 Default로 아래의 주기로 데이터를 받아옵니다.
1. JMX 데이터는 1분
2. ConsumerGroup 1분
3. ISR 1분
4. 토픽 증분 1시간

### Output
현재는 Influx DB에만 Output write가 가능합니다.
