#!/usr/bin/env bash

BASE_DIR=$(cd `dirname ${BASH_SOURCE[0]}`/.. && pwd)
CONF_PATH=${BASE_DIR}/conf/meta.properties
LOG_PATH=-Dlog4j.configuration=file:${BASE_DIR}/conf/log4j.properties
BIN_DIR=${BASE_DIR}/bin
LIB_DIR=${BASE_DIR}/lib

if [ x"" == x${JAVA_HOME} ]; then
    JAVA=java
else
    JAVA=${JAVA_HOME}/bin/java
fi

for l in `ls -d ${LIB_DIR}/*`
do
    CLASSPATH=${CLASSPATH}:$l
done

nohup $JAVA -cp $CLASSPATH $LOG_PATH -Xms256m -Xmx512m com.toss.transfer.JmxZkTransfer $CONF_PATH $BASE_DIR $@ 1> /dev/null 2>&1 &