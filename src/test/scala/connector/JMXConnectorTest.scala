package connector

import com.toss.transfer.connector.{JMXConnectFactory, Queries, Server}
import com.toss.transfer.util.JsonUtils
import javax.management.Query
import org.junit.Test

/**
  * Created by 1002718 on 2017. 2. 8..
  */
class JMXConnectorTest {

  @Test
  def preQuery = {
    assert(JMXConnectFactory.preQuery("kafka.server:type=BrokerTopicMetrics,name=BytesInPerSec,topic=ABC") == ("kafka.server:type=BrokerTopicMetrics,name=BytesInPerSec,topic=ABC", false))
    assert(JMXConnectFactory.preQuery("kafka.server:type=BrokerTopicMetrics,name=BytesInPerSec,topic=*") == ("kafka.server:type=BrokerTopicMetrics,name=BytesInPerSec,topic=*", true))
    assert(JMXConnectFactory.preQuery("kafka.server:type=BrokerTopicMetrics,name=BytesInPerSec,topic=*,partition=*") == ("kafka.server:type=BrokerTopicMetrics,name=BytesInPerSec,topic=*", true))
    assert(JMXConnectFactory.preQuery("kafka.server:type=BrokerTopicMetrics,name=BytesInPerSec,topic=abc,partition=*") == ("kafka.server:type=BrokerTopicMetrics,name=BytesInPerSec,topic=abc,partition=*", true))
  }

  @Test
  def composite = {
    val server = Server("10000", "localhost", "dd", List(Queries("java.lang:type=MemoryPool,name=G1 Eden Space", List("Usage"), "usage")))
    val con = JMXConnectFactory.connect(server)
    val data = con.get

    print(data)

    data.foreach(d => d.getFields.foreach(f => print (f)))
  }

  @Test
  def test = {
    val path = "/brokers/ids/1"
    val index = path.indexOf('/', 1) match {
      case -1 => path.length
      case x: Int => x
    }
    println(index)
  }

}