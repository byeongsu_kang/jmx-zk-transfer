package com.toss.transfer

import java.util.Properties

import com.toss.transfer.broker.JmxZkTrasnferStartable
import com.toss.transfer.util.{CommandLineUtils, Logging}
import com.toss.transfer.utils.Utils
import joptsimple.OptionParser

/**
  * Created by 1002718 on 2017. 2. 2..
  */
object JmxZkTransfer extends Logging {

  def getPropsFromArgs(args: Array[String]): Properties = {
    val optionParser = new OptionParser

    if (args.length == 0) {
      CommandLineUtils.printUsageAndDie(optionParser, "USAGE: java [options] meta.properties")
    }
    Utils.loadProps(args(0))
  }

  def main(args: Array[String]): Unit = {
    if (args.length != 2) {
      error("Not Enough Arguments")
      System.exit(1)
    }

    getPropsFromArgs(args)
    try {
      val serverProps = getPropsFromArgs(args)
      val basedir = args(1)
      val app = JmxZkTrasnferStartable.fromProps(serverProps, basedir)

      Runtime.getRuntime.addShutdownHook(new Thread("BatchSchedular Shutdown") {
        override def run() {
          info("Call Shutdown Hook")
          app.shutdown
        }
      })

      app.startup
    }
    catch {
      case e: Throwable =>
        fatal(e)
        System.exit(1)
    }
    System.exit(0)
  }
}
