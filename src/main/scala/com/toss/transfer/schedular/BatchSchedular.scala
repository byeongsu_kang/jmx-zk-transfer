package com.toss.transfer.schedular

import java.util.Properties
import java.util.concurrent.{CountDownLatch, ScheduledThreadPoolExecutor, TimeUnit}

import com.toss.transfer.connector._
import com.toss.transfer.util.{ApplicationProperties, Logging, ZKUtils}


/**
  * Created by 1002718 on 2017. 2. 2..
  */
object BatchSchedular {
}

class BatchSchedular extends Logging {
  private var threadList: Seq[JobThread] = Nil
  private var endOffset: Map[String, Long] = Map()

  def startup(jobList: Map[String, Int], applicationProperties: ApplicationProperties): Unit = {
    threadList = jobList.map(jobs => new JobThread(jobs._1, jobs._2, applicationProperties, endOffset)).toSeq
    threadList.foreach(_.startup)
    threadList.foreach(_.awaitShutdown())
  }

  def shutdown(): Unit = {
    cleanShutdown()
  }


  def cleanShutdown(): Unit = {
    info("Start clean shutdown.")
    // Shutdown consumer threads.

    JMXConnectFactory.close
    ZKConnectFactory.close
    info("Shutting down threads.")
    if (threadList != null) {
      threadList.foreach(_.shutdown())
      threadList.foreach(_.awaitShutdown())
    }
    info("Thread All Down")
  }
}

class JobThread(jobName: String, interval: Int, props: ApplicationProperties, endOffset: Map[String, Long]) extends Logging {
  private val threadName = "job-thread-" + jobName
  private val shutdownLatch: CountDownLatch = new CountDownLatch(1)
  val servers: Seq[Servers] = props.jmxSetting
  @volatile private var shuttingDown: Boolean = false
  this.logIdentity = "[%s] ".format(threadName)

  val threadTask = new Thread {
    setName(threadName)
    info(getName + "Thread Job Start")
    println(getName + "Thread Job Start")

    override def run() {
      try {
        if (!shuttingDown) {
          jobName match {
            case _ if ApplicationProperties.TOPIC_INCREMENT == jobName => {
              props.ZK_CONNECTION_PATH.foreach(zkInfo => {
                try {
                  val con = ZKConnectFactory.connect(zkInfo)
                  info(s"jobName ${ApplicationProperties.TOPIC_INCREMENT}, IP : ${zkInfo.path}, output : ${zkInfo.output}")
                  val data = ZKUtils.getTopicIncrement(con)
                  val output = zkInfo.output
                  props.WRITER_CLASS.write(output.target, data)
                } catch {
                  case e: Exception => error(s"Error In TopicIncrement : $e")
                }
              })
            }
            case _ if ApplicationProperties.CONSUMER_GROUP == jobName => {
              props.ZK_CONNECTION_PATH.foreach(zkInfo => {
                try {
                  val con = ZKConnectFactory.connect(zkInfo)
                  info(s"jobName ${ApplicationProperties.CONSUMER_GROUP}, IP : ${zkInfo.path}, output : ${zkInfo.output}")
                  val output = zkInfo.output
                  val data = ZKUtils.getConsumerGroup(con)
                  props.WRITER_CLASS.write(output.target, data)

                } catch {
                  case e: Exception => error(s"Error In ConsumerGroup : $e")
                }
              })
            }
            case _ if ApplicationProperties.ISR == jobName => {
              props.ZK_CONNECTION_PATH.foreach(zkInfo => {
                try {
                  val con = ZKConnectFactory.connect(zkInfo)
                  info(s"jobName ${ApplicationProperties.ISR}, IP : ${zkInfo.path}, output : ${zkInfo.output}")
                  val data = ZKUtils.getISR(con)
                  val output = zkInfo.output
                  props.WRITER_CLASS.write(output.target, data)
                } catch {
                  case e: Exception => error(s"Error In ISR : $e")
                }
              })
            }
            case _ if ApplicationProperties.JMX == jobName => {
              servers.foreach(serverList =>
                serverList.servers.foreach(server =>
                  try {
                    val con = JMXConnectFactory.connect(server)
                    val data = con.get
                    val output = serverList.output
                    props.WRITER_CLASS.write[String, Double](output.target, data)
                  } catch {
                    case e: Exception => {
                      error(s"Error In JMX : $e")
                      JMXConnectFactory.removeConnectionPool(server)
                    }
                  }
                ))
            }
            case _ if ApplicationProperties.SPARK_METRICS == jobName => {
              try {
                val con = new SparkConnect(props.SPARK_JOBS_URL, props.SPARK_METRIC_URL)
                val data = con.get
                props.WRITER_CLASS.write[String, Double]("spark-metrics", data)
              } catch {
                case e: Exception => error(s"Error In Spark Metrics : $e")
              }
            }
            case _ if ApplicationProperties.KUDU_METRICS == jobName => {
              try {
                val con = new KuduConnector(props.KUDU_METRIC_URL)
                val data = con.get

                props.WRITER_CLASS.write[String, Double]("KuduMetrics", data)
              } catch {
                case e: Exception => error(s"Error In Kudu Metrics : $e")
              }
            }
            /*case _ if ApplicationProperties.NEW_CONSUMER_GROUP_OFFSET == jobName => {

              // 생성 및 초기화 offset토픽 추가해야할듯
              val consumers = props.KAFKA_CONNECTION_PATH.map(kafka => {
                info(kafka.alias + "-" + kafka.bootstrapServer + "-" + kafka.target)
                val consumer = new KafkaConsumer[Array[Byte], Array[Byte]](getConsumerProps(kafka.bootstrapServer, kafka.alias + "-" + kafka.target + "_new_consumer_offset-dc1"))
                consumer.subscribe(util.Arrays.asList(kafka.offsetTopic))
                (kafka.target, consumer)
              })


              while (true) {
                consumers.foreach(consumerOps => {
                  try {
                    val target = consumerOps._1
                    val consumer = consumerOps._2
                    val records = consumer.poll(1000)
                    import scala.collection.JavaConversions._
                    val res = records.filter(msg => msg.key != null && msg.value != null).map(msg => {
                      val optData = GroupMetadataManager.readMessageKey(ByteBuffer.wrap(msg.key)) match {
                        case groupTopicPartition: OffsetKey =>

                          val formattedValue = GroupMetadataManager.readOffsetMessageValue(ByteBuffer.wrap(msg.value))
                          val group = groupTopicPartition.key.group
                          val tp = groupTopicPartition.key.topicPartition
                          val topic = tp.topic
                          val partition = tp.partition
                          val offset = formattedValue.offset
                          val tags = Map[String, String]("consumergroup" -> group, "topic" -> topic, "partition" -> partition.toString)
                          val fields = Map[String, Double]("offset" -> offset.toDouble)

                          new FormattedMessage[String, Double]("NewConsumerGroup", tags, fields)
                        case _ => new FormattedMessage[String, Double]("NewConsumerGroup", Map[String, String](), Map[String, Double]())
                      }

                      optData

                    }).toSeq
                    props.WRITER_CLASS.write[String, Double](target, res)
                  } catch {
                    case e: Exception =>
                      error("ERROR in middle of Consume. " + e.getMessage)
                  }
                })
//                Thread.sleep(30000)
              }
            }*/
            case _ => warn("who are you?" + getName)
          }
        }
      } catch {
        case t: Throwable =>
          fatal(jobName + "Schedule thread failure due to ", t)
      }
    }
  }

  def getConsumerProps(path: String, groupName: String): Properties = {
    val consumerProperties = new Properties
    consumerProperties.setProperty("bootstrap.servers", path)
    consumerProperties.setProperty("group.id", groupName)
    consumerProperties.setProperty("enable.auto.commit", "true")
    consumerProperties.setProperty("auto.commit.interval.ms", "60000")
    consumerProperties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer")
    consumerProperties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer")
    consumerProperties.setProperty("exclude.internal.topics", "false")
    consumerProperties.setProperty("socket.timeout.ms", "30000")
    consumerProperties.setProperty("socket.receive.buffer.bytes", "65536")
    consumerProperties.setProperty("max.partition.fetch.bytes", "2097152")
    consumerProperties.setProperty("max.poll.records", "10000")
    consumerProperties.setProperty("auto.offset.reset", "earliest")
    consumerProperties.setProperty("rebalance.max.retries", "4")
    consumerProperties.setProperty("fetch.min.bytes", "1")
    consumerProperties
  }

  def shutdown(): Unit = {
    shuttingDown = true
    shutdownLatch.countDown()
  }

  def startup = {
    if (interval != -1) {
      val ex = new ScheduledThreadPoolExecutor(1)
      ex.scheduleAtFixedRate(threadTask, 1, interval, TimeUnit.SECONDS)
    } else {
      threadTask.start()
    }
  }

  def awaitShutdown() {
    try {
      shutdownLatch.await()
      info("Process thread shutdown complete")
    } catch {
      case ie: InterruptedException =>
        warn("Shutdown of the process interrupted")
    }
  }
}