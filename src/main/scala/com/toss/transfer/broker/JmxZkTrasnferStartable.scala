package com.toss.transfer.broker

import java.util.Properties

import com.toss.transfer.schedular.BatchSchedular
import com.toss.transfer.util.{ApplicationProperties, Logging}

/**
  * Created by 1002718 on 2017. 2. 11..
  */
object JmxZkTrasnferStartable extends Logging {

  def fromProps(serverProps: Properties, basedir: String) = {
    new JmxZkTrasnferStartable(ApplicationProperties.fromProps(serverProps, basedir))
  }
}

class JmxZkTrasnferStartable(props: ApplicationProperties) extends Logging {

  private val app = new BatchSchedular()
  private val jobList = Map(
//    ApplicationProperties.CONSUMER_GROUP -> ApplicationProperties.CONSUMER_GROUP_OFFSET_JOB_INTERVAL,
    ApplicationProperties.JMX -> ApplicationProperties.JMX_JOB_INTERVAL
//    ApplicationProperties.NEW_CONSUMER_GROUP_OFFSET -> -1
//    ApplicationProperties.KUDU_METRICS -> ApplicationProperties.KUDU_METRICS_INTERVAL
  )

  def startup() {
    try {
      app.startup(jobList, props)
    }
    catch {
      case e: Throwable =>
        fatal("Fatal error during KafkaServerStartable startup. Prepare to shutdown", e)
        // KafkaServer already calls shutdown() internally, so this is purely for logging & the exit code
        System.exit(1)
    }
  }

  def shutdown() {
    try {
      app.shutdown
    }
    catch {
      case e: Throwable =>
        fatal("Fatal error during KafkaServerStable shutdown. Prepare to halt", e)
        // Calling exit() can lead to deadlock as exit() can be called multiple times. Force exit.
        Runtime.getRuntime.halt(1)
    }
  }
}
