package com.toss.transfer.database.influxdb

import java.util.{Date, Properties}

import com.toss.transfer.connector.formatter.FormattedMessage
import com.toss.transfer.util.Logging
import org.apache.kafka.clients.producer.{Callback, KafkaProducer, ProducerRecord, RecordMetadata}
import org.json4s.DefaultFormats
import org.json4s.native.Json

/**
  * Created by 1002718 on 2017. 12. 22..
  */
class KafkaWriter(url: String) extends Writer with Logging {

  import java.text.SimpleDateFormat

  val f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
  val kafkaProducer: KafkaProducer[String, String] = {
    val props = new Properties()
    props.setProperty("bootstrap.servers", url)
    props.setProperty("acks", "all")
    props.setProperty("retries", "1000000")
    props.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.setProperty("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.setProperty("request.timeout.ms", "60000")
    new KafkaProducer[String, String](props)
  }

  override def write[K, V](target: String, data: Seq[FormattedMessage[K, V]]): Unit = {
    data.filter(res => res.getFields.nonEmpty && res.getTags.nonEmpty).foreach(value => {

      val m = Map("log_time" -> f.format(new Date(System.currentTimeMillis() - 32400 * 1000)), "measurement" -> value.getMeasurement) ++ value.getTags ++ value.getFields
      val message = Json(DefaultFormats).write(m)

      kafkaProducer.send(new ProducerRecord[String, String](target, message), new Callback() {
        override def onCompletion(recordMetadata: RecordMetadata, e: Exception): Unit = {
          if (e != null) {
            error("Send Callback with Error. " + e.getMessage)
          }
        }
      })
    })
    kafkaProducer.flush()
  }
}
