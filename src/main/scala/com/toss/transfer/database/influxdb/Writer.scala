package com.toss.transfer.database.influxdb

import com.toss.transfer.connector.formatter.FormattedMessage

/**
  * Created by 1002718 on 2017. 12. 22..
  */
trait Writer {
  def write[K, V](target: String, data: Seq[FormattedMessage[K, V]]): Unit
}
