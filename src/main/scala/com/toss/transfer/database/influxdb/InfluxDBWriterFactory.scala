package com.toss.transfer.database.influxdb

import org.influxdb.{InfluxDB, InfluxDBFactory}

/**
  * Created by 1002718 on 2017. 2. 13..
  */
object InfluxDBWriterFactory {
  def create(url: String, username: String, password: String): InfluxDB = {
    val connect = InfluxDBFactory
      .connect(url, username, password)
    connect
  }
}
