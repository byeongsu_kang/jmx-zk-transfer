package com.toss.transfer.database.influxdb

import java.util.concurrent.TimeUnit

import com.toss.transfer.connector.formatter.FormattedMessage
import org.influxdb.InfluxDB
import org.influxdb.dto.{BatchPoints, Point}

/**
  * Created by 1002718 on 2017. 12. 22..
  */
class InfluxDBWriter(url: String, username: String, password: String) extends Writer {
  val influxConnector = InfluxDBWriterFactory.create(url, username, password)
  private val RETENTION_POLICY = "autogen"

  def makeBatch(database: String, retentionPolicy: String): BatchPoints = {
    val temp = BatchPoints.database(database)
    temp.retentionPolicy(retentionPolicy).consistency(InfluxDB.ConsistencyLevel.ALL).build
  }

  def addDoubleFields(measurement: String, tags: Map[String, String], fields: Map[String, Double]): Point = {

    if (fields != null && fields.nonEmpty) {
      val temp = Point.measurement(measurement).time(System.currentTimeMillis, TimeUnit.MILLISECONDS)
      if (tags != null && tags.nonEmpty)
        tags.filter(_._2 != null).map(test => temp.tag(test._1, test._2))
      fields.foreach(field => temp.addField(field._1, field._2))
      temp.build
    } else
      null
  }

  def addStringFields(measurement: String, tags: Map[String, String], fields: Map[String, String]): Point = {
    if (fields != null && fields.nonEmpty) {
      val temp = Point.measurement(measurement).time(System.currentTimeMillis, TimeUnit.MILLISECONDS)
      if (tags != null && tags.nonEmpty)
        tags.filter(_._2 != null).map(test => temp.tag(test._1, test._2))
      fields.foreach(field => temp.addField(field._1, field._2))
      temp.build
    } else
      null
  }


  def whatUrType[K, V](data: Iterable[V]): Int = {
    var myType = 0
    data.foreach(value => {
      if (value.isInstanceOf[Double]) {
        myType = 1
      } else {
        myType = 0
      }
    })
    myType
  }

  override def write[K, V](target: String, data: Seq[FormattedMessage[K, V]]): Unit = {
    try {
      val batchPoints = makeBatch(target, RETENTION_POLICY)
      val points: Seq[Point] = data.filter(res => res.getFields.nonEmpty && res.getTags.nonEmpty).map(res => {
        val myType = whatUrType[K, V](res.getFields.values)

        myType match {
          case 0 =>
            addStringFields(res.getMeasurement, res.getTags, res.getFields.asInstanceOf[Map[String, String]])
          case 1 =>
            addDoubleFields(res.getMeasurement, res.getTags, res.getFields.asInstanceOf[Map[String, Double]])
        }
      })

      if (points != null && points.nonEmpty && points.last != null) {
        points.foreach(point => batchPoints.point(point))
        influxConnector.write(batchPoints)
      }
    } catch {
      case e: Exception => {
        error(s"$target , ${data.last.getMeasurement} - ${data.last.getTags.toString} - ${data.last.getFields.toString} Write Error : $e")
      }
    }
  }
}
