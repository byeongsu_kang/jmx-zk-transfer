package com.toss.transfer.util

import java.util.Properties

import com.toss.transfer.connector._
import com.toss.transfer.database.influxdb.{InfluxDBWriter, KafkaWriter, Writer}
import org.I0Itec.zkclient.exception.ZkTimeoutException


/**
  * Created by 1002718 on 2017. 2. 3..
  */
object ApplicationProperties {

  def fromProps(props: Properties, basedir: String): ApplicationProperties = {
    this.JMX_QUERY_DIR_PATH = basedir + "/conf/jmx-servers/queries/"
    this.JMX_CONFIG_DIR_PATH = "/conf/jmx-servers/"
    this.ZK_CONFIG_DIR_PATH = "/conf/zookeepers/"
    this.KAFKA_CONFIG_DIR_PATH = "/conf/kafka/"
    this.fromProps(props, basedir, true)
  }

  def fromProps(props: Properties, basedir: String, doLog: Boolean): ApplicationProperties = {
    val applicationProps = new ApplicationProperties(props, basedir, doLog)
    applicationProps.loadZK
//    applicationProps.loadKafka
    applicationProps.initWriter
    applicationProps
  }

  //Default Directory Path
  var JMX_QUERY_DIR_PATH = ""
  var JMX_CONFIG_DIR_PATH = ""
  var ZK_CONFIG_DIR_PATH = ""
  var KAFKA_CONFIG_DIR_PATH = ""


  //JOB Name
  val TOPIC_LIST = "topiclist"
  val TOPIC_INCREMENT = "topicincrement"
  val CONSUMER_GROUP = "consumergroup"
  val NEW_CONSUMER_GROUP_OFFSET = "new_consumer_group_offset"
  val SPARK_METRICS = "spark"
  val KUDU_METRICS = "kudu"
  val ISR = "isr"
  val JMX = "jmx"
  //Time Pattern
  val TIME_PATTERN = "yyyyMMddHHmmssmmm"


  //Schedule Interval seconds
  val TOPIC_LIST_JOB_INTERVAL = 60 * 60
  val TOPIC_INCREMENT_JOB_INTERVAL = 60 * 60
  val CONSUMER_GROUP_OFFSET_JOB_INTERVAL = 60
  val ISR_JOB_INVERVAL = 60
  val JMX_JOB_INTERVAL = 60
  val SPARK_METRICS_INTERVAL = 60
  val KUDU_METRICS_INTERVAL = 5

  //JMX
  val JMX_RMI_FORMAT = "service:jmx:rmi:///jndi/rmi://%s/jmxrmi"

  val JSON_REGEX = "[^:\\s?.]*.json\"".r


}

class ApplicationProperties(props: Properties, basedir: String, doLog: Boolean) extends Logging {

  def initWriter(): Unit = {
    info("Writers... " + props.get("writer"))
    props.get("writer").toString match {
      case "influx" =>
        WRITER_CLASS = new InfluxDBWriter(props.get("writer.url").toString, props.get("writer.username").toString, props.get("writer.password").toString)
      case "kafka" =>
        WRITER_CLASS = new KafkaWriter(props.get("writer.url").toString)
    }
  }

  def loadZK(): Unit = {
    if (props.containsKey("zookeeper.list")) {
      val zkList = props.get("zookeeper.list")
      ZK_CONNECTION_PATH = zkList.toString.split(",").toSeq.map(filename => {
        val zkInfo = JsonUtils.zkParser(Tools.fileReader(basedir + ApplicationProperties.ZK_CONFIG_DIR_PATH + filename))
        if (!checkZKPath(zkInfo)) {
          error("ZK Connection Exception. ZK Path is " + zkInfo.path + "\n Process Close")
          System.exit(1)
        }
        zkInfo
      })
    }
  }

  def loadKafka(): Unit = {
    if (props.containsKey("kafka.list")) {
      val kafkaList = props.get("kafka.list").toString
      KAFKA_CONNECTION_PATH = kafkaList.split(",").toSeq.map(filename => {
        JsonUtils.kafkaParser(Tools.fileReader(basedir + ApplicationProperties.KAFKA_CONFIG_DIR_PATH + filename))
      })
    }
  }


  def checkZKPath(zkInfo: ZKInfo): Boolean = {
    info("Connecting to zookeeper on " + zkInfo.path)
    val chroot = {
      if (zkInfo.path.indexOf("/") > 0)
        zkInfo.path.substring(zkInfo.path.indexOf("/"))
      else
        ""
    }
    val zkConnPath = if (chroot == "") zkInfo.path else zkInfo.path.substring(0, zkInfo.path.indexOf("/"))

    val zkClient = try {
      val zkClientForChrootCreation = ZKUtils.createZkClientAndConnection(zkConnPath,
        zkInfo.sessionTimeout,
        zkInfo.connectionTimeout)

      zkClientForChrootCreation
    } catch {
      case e: ZkTimeoutException => {
        error("ZK Connection ERROR. Process shutdown\n" + e)
        null
      }
      case e: Exception =>
        error("ZK Exception\n" + e)
        null
    }

    val conIsValid = if (zkClient == null) false else true
    val chrootIsValid = if (chroot.length > 1) ZKUtils.pathExists(zkClient, chroot) else true

    zkClient.close()

    conIsValid & chrootIsValid
  }


  def jmxSetting: Seq[Servers] = {
    if (props.containsKey("jmx.server.list")) {
      val jmxList = props.get("jmx.server.list")

      val serverList = jmxList.toString.split(",").toSeq
        .map(a => Tools.fileReader(basedir + ApplicationProperties.JMX_CONFIG_DIR_PATH + a))
        .map(b => {
          val queried = Tools.queryMaker(b)
          JsonUtils.parser(queried)
        })
      serverList
    } else {
      List()
    }
  }

  var JMX_SERVER_LIST: List[Server] = Nil
  var ZK_CONNECTION_PATH: Seq[ZKInfo] = Nil
  var KAFKA_CONNECTION_PATH: Seq[Kafka] = Nil
  var SPARK_JOBS_URL: String = "http://dicc-m004:8088/ws/v1/cluster/apps?state=running&applicationTypes=SPARK"
  var SPARK_METRIC_URL: String = "http://dicc-m004:8088/proxy/%s/api/v1/applications/%s/streaming/statistics"
  val KUDU_METRIC_URL: String = "http://192.168.112.204:8050/metrics?metrics=connections_accepted,generic_current_allocated_bytes,tcmalloc_pageheap_unmapped_bytes,tcmalloc_max_total_thread_cache_bytes,tcmalloc_current_total_thread_cache_bytes,generic_heap_size,tcmalloc_pageheap_free_bytes,log_block_manager_bytes_under_management,log_block_manager_blocks_under_management,log_block_manager_full_containers,log_block_manager_containers,block_manager_total_disk_sync,block_manager_blocks_open_reading,block_manager_blocks_open_writing,block_manager_total_writable_blocks,block_manager_total_readable_blocks,block_manager_total_bytes_written,block_manager_total_bytes_read,rpcs_timed_out_in_queue,rpcs_queue_overflow,block_cache_inserts,block_cache_lookups,block_cache_evictions,block_cache_misses,block_cache_misses_caching,block_cache_hits,block_cache_hits_caching,block_cache_usage,spinlock_contention_time,tcmalloc_contention_time,threads_started,threads_running"
  val sessionTimeout = 30000
  val connectionTimeout = 30000

  var WRITER_CLASS: Writer = _

}