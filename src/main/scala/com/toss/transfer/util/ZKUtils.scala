package com.toss.transfer.util

import java.io.File

import com.toss.transfer.connector.formatter.FormattedMessage
import org.I0Itec.zkclient.{ZkClient, ZkConnection}
import org.I0Itec.zkclient.exception.{ZkMarshallingError, ZkNoNodeException}
import org.I0Itec.zkclient.serialize.ZkSerializer
import org.apache.zookeeper.data.Stat

import scala.collection.Seq

/**
  * Created by 1002718 on 2017. 4. 3..
  */
object ZKUtils {
  //Zookeeper Path Properties
  val ZK_CONSUMER_PATH = "/consumers"
  val ZK_BROKER_PATH = "/brokers"
  val ZK_CONTROLLER_PATH = "/controller"
  val ZK_CONTROLLER_EPOCH_PATH = "/controller_epoch"
  val ZK_CONFIG_PATH = "/config"
  val ZK_ISR_NOTI_PATH = "/isr_change_notification"
  val ZK_ADMIN_PATH = "/admin"
  val ZK_REASSIGN_PARTITIONS_PATH = ZK_ADMIN_PATH + "/reassign_partitions"
  val ZK_DELETE_TOPICS_PATH = ZK_ADMIN_PATH + "/delete_topics"
  val ZK_PREFERRED_REPLICA_ELECTION_PATH = ZK_ADMIN_PATH + "/preferred_replica_election"
  val ZK_SEQID_PATH = ZK_BROKER_PATH + "/seqid"
  val ZK_TOPICS_PATH = ZK_BROKER_PATH + "/topics"
  val EntityConfigChangesPath = ZK_CONFIG_PATH + "/changes"


  def readDataMaybeNull(zkClient: ZkClient, path: String): (Option[String], Stat) = {
    val stat: Stat = new Stat()
    val dataAndStat = try {
      (Some(zkClient.readData(path, stat)), stat)
    } catch {
      case e: ZkNoNodeException => {
        error("Zk Read Data No node :" + e)
        (None, stat)
      }
      case e2: Throwable => throw e2
    }
    dataAndStat
  }

  def getChildrenParentMayNotExist(zkClient: ZkClient, path: String): Seq[String] = {
    import scala.collection.JavaConversions._
    // triggers implicit conversion from java list to scala Seq
    try {
      zkClient.getChildren(path)
    } catch {
      case e: ZkNoNodeException => {
        error("Zk getChildren No node :" + e)
        Nil
      }
      case e2: Throwable => throw e2
    }
  }

  def pathExists(zkClient: ZkClient, path: String): Boolean = {
    zkClient.exists(path)
  }

  def createZkClientAndConnection(zkUrl: String, sessionTimeout: Int, connectionTimeout: Int): ZkClient = {
    val zkConnection = new ZkConnection(zkUrl, sessionTimeout)
    val zkClient = new ZkClient(zkConnection, connectionTimeout, ZKStringSerializer)
    zkClient
  }

  def getTopicIncrement(zkClient: ZkClient): Seq[FormattedMessage[String, String]] = {
    val topicList = ZKUtils.getChildrenParentMayNotExist(zkClient, ZKUtils.ZK_TOPICS_PATH)
    val todayCreation = topicList.map(topic => (topic, ZKUtils.readDataMaybeNull(zkClient, ZKUtils.ZK_TOPICS_PATH + "/" + topic)._2.getCtime)).filter(_._2 > System.currentTimeMillis() - (1000 * ApplicationProperties.TOPIC_INCREMENT_JOB_INTERVAL) /*현재시간 - 이전 배치시간 < 생성시간*/)

    todayCreation.map(values => {
      val tags = Map("topic" -> values._1)
      val field = Map("topic" -> values._1, "creationTime" -> values._2.toString)
      new FormattedMessage("TopicIncrement", tags, field)
    })
  }

  def getConsumerGroup(zkClient: ZkClient): Seq[FormattedMessage[String, Double]] = {
    ZKUtils.getChildrenParentMayNotExist(zkClient, ZKUtils.ZK_CONSUMER_PATH).filter(cg => ZKUtils.getChildrenParentMayNotExist(zkClient, ZKUtils.ZK_CONSUMER_PATH + File.separator + cg).contains("offsets") && !cg.startsWith("console-consumer"))
      .flatMap(cg =>
        ZKUtils.getChildrenParentMayNotExist(zkClient, ZKUtils.ZK_CONSUMER_PATH + File.separator + cg + "/offsets")
          .flatMap(topic => ZKUtils.getChildrenParentMayNotExist(zkClient, ZKUtils.ZK_CONSUMER_PATH + File.separator + cg + "/offsets/" + topic)
            .map(partition => {
              val tags = Map("consumergroup" -> cg, "topic" -> topic, "partition" -> partition.toString)
              val offset = ZKUtils.readDataMaybeNull(zkClient, ZKUtils.ZK_CONSUMER_PATH + File.separator + cg + "/offsets/" + topic + File.separator + partition)._1 match {
                case Some(offset) => offset
                case None => "-1"
              }
              val fields = Map("offset" -> offset.toDouble)
              new FormattedMessage("ConsumerGroup", tags, fields)
            })))
  }

  def getISR(zkClient: ZkClient): Seq[FormattedMessage[String, String]] = {
    ZKUtils.getChildrenParentMayNotExist(zkClient, ZKUtils.ZK_TOPICS_PATH).filterNot(_ == "__consumer_offsets").flatMap({
      topic =>
        ZKUtils.getChildrenParentMayNotExist(zkClient, ZKUtils.ZK_TOPICS_PATH + File.separator + topic + "/partitions")
          .map(partition => {
            val isr = ZKUtils.readDataMaybeNull(zkClient, ZKUtils.ZK_TOPICS_PATH + File.separator + topic + "/partitions/" + partition + "/state")._1 match {
              case Some(isr) => isr
              case None => ""
            }
            val replication = ZKUtils.readDataMaybeNull(zkClient, ZKUtils.ZK_TOPICS_PATH + File.separator + topic)._1 match {
              case Some(json) =>
                json
              case None => ""
            }

            val child = (JsonUtils.plainParser(replication) \\ "partitions" \\ partition).children
            val isrObj = JsonUtils.isrParser(isr)

            val isrNotInSyncSize = child.map(ch => {
              val target = isrObj.isr
              val source = JsonUtils.JInttoInt(ch)
              target.contains(source)
            }).filter(_ == false)

            val rep = child.map(ch => JsonUtils.JInttoInt(ch)).mkString(",")

            var status = true
            if (isrNotInSyncSize.nonEmpty)
              status = false
            val isrStr = isrObj.isr.mkString(",")

            val tags = Map("status" -> status.toString, "topic" -> topic, "partition" -> partition)
            val fields = Map("topic" -> topic, "partition" -> partition, "replication" -> rep, "isr" -> isrStr)
            new FormattedMessage("ISR", tags, fields)
          })
    })
  }
}

object ZKStringSerializer extends ZkSerializer {

  @throws(classOf[ZkMarshallingError])
  def serialize(data: Object): Array[Byte] = data.asInstanceOf[String].getBytes("UTF-8")

  @throws(classOf[ZkMarshallingError])
  def deserialize(bytes: Array[Byte]): Object = {
    if (bytes == null)
      null
    else
      new String(bytes, "UTF-8")
  }
}
