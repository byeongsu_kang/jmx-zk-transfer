package com.toss.transfer.util

import org.apache.log4j.Logger


/**
  * Created by 1002718 on 2016. 12. 6..
  */
trait Logging {
  val loggerName = this.getClass.getName;
  lazy val logger = Logger.getLogger(loggerName)

  protected var logIdentity : String = null;

  private def msgWithLogIdent(msg:String)=if(logIdentity == null) msg else logIdentity+msg

  def debug(msg: => String): Unit = {
    if (logger.isDebugEnabled())
      logger.debug(msgWithLogIdent(msg))
  }
  def debug(e: => Throwable): Any = {
    if (logger.isDebugEnabled())
      logger.debug(logIdentity,e)
  }
  def debug(msg: => String, e: => Throwable) = {
    if (logger.isDebugEnabled())
      logger.debug(msgWithLogIdent(msg),e)
  }

  def info(msg: => String): Unit = {
    if (logger.isInfoEnabled())
      logger.info(msgWithLogIdent(msg))
  }
  def info(e: => Throwable): Any = {
    if (logger.isInfoEnabled())
      logger.info(logIdentity,e)
  }
  def info(msg: => String,e: => Throwable) = {
    if (logger.isInfoEnabled())
      logger.info(msgWithLogIdent(msg),e)
  }


  def warn(msg: => String): Unit = {
    logger.warn(msgWithLogIdent(msg))
  }
  def warn(e: => Throwable): Any = {
    logger.warn(logIdentity,e)
  }
  def warn(msg: => String, e: => Throwable) = {
    logger.warn(msgWithLogIdent(msg),e)
  }

  def error(msg: => String): Unit = {
    logger.error(msgWithLogIdent(msg))
  }
  def error(e: => Throwable): Any = {
    logger.error(logIdentity,e)
  }
  def error(msg: => String, e: => Throwable) = {
    logger.error(msgWithLogIdent(msg),e)
  }


  def fatal(msg: => String): Unit = {
    logger.fatal(msgWithLogIdent(msg))
  }
  def fatal(e: => Throwable): Any = {
    logger.fatal(logIdentity,e)
  }
  def fatal(msg: => String, e: => Throwable) = {
    logger.fatal(msgWithLogIdent(msg),e)
  }
}