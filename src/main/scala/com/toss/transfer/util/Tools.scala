package com.toss.transfer.util

import java.text.SimpleDateFormat

import org.joda.time.format.DateTimeFormat

import scala.io.{BufferedSource, Source}

/**
  * Created by 1002718 on 2017. 2. 3..
  */
object Tools extends Logging {


  def convertDate(ctime: Long): String = {
    DateTimeFormat.forPattern(ApplicationProperties.TIME_PATTERN).print(ctime)
  }

  def todayMilliseconds(): Long = {
    val today = DateTimeFormat.forPattern("yyyyMMdd").print(System.currentTimeMillis()) + "000000000"
    val sdf = new SimpleDateFormat("yyyyMMddHHmmssmmm");
    sdf.parse(today).getTime
  }

  def fileReader(path: String): String = {
    val reader = Source.fromFile(path)
    val data = reader.getLines.mkString
    frClose(reader)
    data
  }

  def frClose(fr: BufferedSource) = {
    fr.close
  }

  def queryMaker(source: String): String = {
    val firstMatched = ApplicationProperties.JSON_REGEX findFirstMatchIn source
    firstMatched match {
      case Some(matched) => {
        val queries = Source.fromFile(ApplicationProperties.JMX_QUERY_DIR_PATH + matched.toString.replace("\"", "")).getLines().mkString
        queryMaker(source.replace(matched.toString.trim, queries))
      }
      case None => {
        source
      }
    }
  }
}