package com.toss.transfer.util

import com.toss.transfer.connector._
import org.json4s.{CustomSerializer, DefaultFormats, JObject}
import org.json4s.JsonAST.JValue
import org.json4s.native.JsonMethods.parse

/**
  * Created by 1002718 on 2017. 2. 11..
  */
object JsonUtils extends Logging {
  implicit val formats = DefaultFormats

  def parser(text: String): Servers = {
    try {
      parse(text).extract[Servers]
    } catch {
      case e: Exception => {
        error("Server Mapping Error" + e)
        null
      }
    }
  }

  def zkParser(text: String): ZKInfo = {
    try {
      parse(text).extract[ZKInfo]
    } catch {
      case e: Exception => {
        error(e)
        null
      }
    }
  }

  def kafkaParser(text: String): Kafka = {
    try {
      parse(text).extract[Kafka]
    } catch {
      case e: Exception => {
        error(e)
        null
      }
    }
  }

  def plainParser(text: String): JValue = {
    try {
      parse(text)
    } catch {
      case e: Exception => {
        error(e)
        null
      }
    }
  }

  def isrParser(text: String): ISR = {
    try {
      parse(text).extract[ISR]
    } catch {
      case e: Exception => {
        error(e)
        null
      }
    }
  }


  def JInttoInt(value: JValue): Int = {
    try {
      value.extract[Int]
    } catch {
      case e: Exception => {
        error(e)
        -1
      }
    }
  }


  def sparkParser(text: String): Spark = {
    try {
      parse(text).extract[Spark]
    } catch {
      case e: Exception => {
        error("Apps Mapping Error" + e)
        null
      }
    }
  }

  def kuduParser(text: String): List[Kudu] = {
    try {
      parse(text).extract[List[Kudu]]
    } catch {
      case e: Exception => {
        println("Kudu Mapping Error" + e)
        null
      }
    }
  }

  def sparkMetricsParser(text: String): SparkMetricDetail = {
    try {
      parse(text).extract[SparkMetricDetail]
    } catch {
      case e: Exception =>
        error("Apps Mapping Error" + e)
        null
    }
  }
}