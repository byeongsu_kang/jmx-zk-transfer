package com.toss.transfer.connector

import com.toss.transfer.connector.formatter.FormattedMessage


/**
  * Created by 1002718 on 2017. 2. 2..
  */
trait Connector[K,V] {
  def get() : Seq[FormattedMessage[K,V]]
}
