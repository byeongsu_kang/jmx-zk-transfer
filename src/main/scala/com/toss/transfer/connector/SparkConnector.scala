package com.toss.transfer.connector

import com.toss.transfer.connector.formatter.FormattedMessage
import com.toss.transfer.util.{JsonUtils, Logging}

import scalaj.http.{Http, HttpResponse}

/**
  * Created by 1002718 on 2017. 2. 2..
  */

case class Spark(apps: Apps)

case class Apps(app: List[App])

case class App(id: String, user: String, name: String)

case class SparkMetricDetail(startTime: String, batchDuration: Int, numReceivers: Int, numActiveReceivers: Int, numInactiveReceivers: Int, numTotalCompletedBatches: Int, numRetainedCompletedBatches: Int, numActiveBatches: Int, numProcessedRecords: Int, numReceivedRecords: Int, avgInputRate: Double, avgSchedulingDelay: Int, avgProcessingTime: Int, avgTotalDelay: Int)

class SparkConnect(jobListUrl: String, proxyUrl: String) extends Logging {

  def get: Seq[FormattedMessage[String, Double]] = {
    val response: HttpResponse[String] = Http(jobListUrl).asString

    val obj = JsonUtils.sparkParser(response.body)
    val res = obj.apps.app.filter(sparkJob => sparkJob.user == "bdb").map(sparkJob => {
      try {
        val path = proxyUrl.format(sparkJob.id, sparkJob.id)
        val metricResponse = Http(path).asString

        val result = JsonUtils.sparkMetricsParser(metricResponse.body)

        val tags = Map[String, String]("appId" -> sparkJob.id, "name" -> sparkJob.name, "user" -> sparkJob.user)
        val values = Map[String, Double](
          "batchDuration" -> result.batchDuration,
          "numReceivers" -> result.numReceivers,
          "numActiveReceivers" -> result.numActiveReceivers,
          "numInactiveReceivers" -> result.numInactiveReceivers,
          "numTotalCompletedBatches" -> result.numTotalCompletedBatches,
          "numRetainedCompletedBatches" -> result.numRetainedCompletedBatches,
          "numActiveBatches" -> result.numActiveBatches,
          "numProcessedRecords" -> result.numProcessedRecords,
          "numReceivedRecords" -> result.numReceivedRecords,
          "avgInputRate" -> result.avgInputRate,
          "avgSchedulingDelay" -> result.avgSchedulingDelay,
          "avgProcessingTime" -> result.avgProcessingTime,
          "avgTotalDelay" -> result.avgTotalDelay
        )
        new FormattedMessage[String, Double]("SparkMetrics", tags, values)
      } catch {
        case e: Exception =>
          error(s"Error In getting metrics. ID : ${sparkJob.id}, Name : ${sparkJob.name}, User : ${sparkJob.user}. Error Message : ${e.getMessage}")
          new FormattedMessage[String, Double]("SparkMetrics", Map(), Map())
      }
    })
    res
  }

}
