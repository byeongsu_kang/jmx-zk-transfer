package com.toss.transfer.connector

import com.toss.transfer.connector.formatter.FormattedMessage
import com.toss.transfer.util.{JsonUtils, Logging}

import scalaj.http.{Http, HttpResponse}

/**
  * Created by 1002718 on 2017. 2. 2..
  */

case class Kudu(id: String, metrics: List[KuduMetric])

case class KuduMetric(name: String, value: String)

case class ConvertedMetric(data: String)

class KuduConnector(url: String) extends Logging {

  def get: Seq[FormattedMessage[String, Double]] = {
    val response: HttpResponse[String] = Http(url).asString
    info(response.body)
    val obj = JsonUtils.kuduParser(response.body)
    convertKey(obj)
  }

  def convertKey(obj: List[Kudu]): List[FormattedMessage[String, Double]] = {
    val res = obj.map(kudu => {
      kudu.metrics.flatMap(metric => {
        Map(metric.name -> metric.value)
      })
    })

    val result = res.flatMap(t => {
      //      val values = t.map(c => c._1 -> c._2.toDouble).toMap
      //      val tags = t.map(c => "metric" -> c._1).toMap

      val aa = t.map(test => {
        val values = Map("value" -> test._2.toDouble)
        val tags = Map("metric" -> test._1)
        new FormattedMessage[String, Double]("kudu_metrics", tags, values)

      })
      aa
    })
    result
  }
}
