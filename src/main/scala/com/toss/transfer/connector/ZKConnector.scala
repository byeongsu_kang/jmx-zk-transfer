package com.toss.transfer.connector


import com.toss.transfer.schedular.Database
import com.toss.transfer.util.{Logging, ZKUtils}
import org.I0Itec.zkclient.ZkClient

/**
  * Created by 1002718 on 2017. 2. 2..
  */
case class ZKInfo(path: String, sessionTimeout: Int, connectionTimeout: Int, output: Database)

case class ISR(controller_epoch: Int, leader: Int, version: Int, leader_epoch: Int, isr: Array[Int])

case class Replication(replications: List[Int])

object ZKConnectFactory extends Logging {

  var zkMap = Map[String, ZkClient]()

  def connect(zkInfo: ZKInfo) = {
    val key = zkInfo.path
    info("Zookeeper URL: %s".format(key))
    val zkClient = zkMap.get(key) match {
      case Some(zk) => zk
      case None =>
        val newCon = new ZKConnector(zkInfo).con()
        zkMap += (key -> newCon)
        newCon
    }
    zkClient
  }

  def close = {
    info("ZK Connection Closing")
    zkMap.foreach(zk => zk._2.close())
    info("ZK Connection Closed")
  }
}

class ZKConnector(zkInfo: ZKInfo) {
  def con(): ZkClient = {
    ZKUtils.createZkClientAndConnection(zkInfo.path, zkInfo.sessionTimeout, zkInfo.connectionTimeout)
  }
}


