package com.toss.transfer.connector

import java.util

import com.toss.transfer.connector.formatter.FormattedMessage
import com.toss.transfer.schedular.Database
import com.toss.transfer.util.{ApplicationProperties, Logging}
import javax.management.openmbean.CompositeDataSupport
import javax.management.remote.{JMXConnector, JMXConnectorFactory, JMXServiceURL}
import javax.management.{Attribute, AttributeList, MBeanServerConnection, ObjectName}

import scala.collection.JavaConverters._
import scala.collection.mutable
import java.util.concurrent.{Callable, Executors, TimeUnit}

/**
  * Created by 1002718 on 2017. 2. 2..
  */

case class Servers(servers: List[Server], output: Database)

case class Server(port: String, host: String, alias: String, queries: List[Queries])

case class Kafka(bootstrapServer: String, alias: String, offsetTopic: String, target: String)

case class Queries(obj: String, attr: List[String], resultAlias: String)

case class Meta(measurement: String, hostname: String, tags: mutable.Map[String, String])


object JMXConnectFactory extends Logging {

  var _instances: mutable.Map[String, JMXConnect] = mutable.Map()

  def connectWithTimeout(url: JMXServiceURL, timeout: Long, unit: TimeUnit): JMXConnector = {
    val executor = Executors.newSingleThreadExecutor
    import scala.collection.JavaConverters._

    val env = Map("jmx.remote.x.request.waiting.timeout" -> 5000L).asJava

    val future = executor.submit(new Callable[JMXConnector]() {
      override def call: JMXConnector = JMXConnectorFactory.connect(url, env)
    })
    future.get(timeout, unit)
  }

  def connect(server: Server): JMXConnect = {
    val key = getKey(server)
    info("JMX URL: %s".format(key))
    if (!_instances.contains(key)) {

      //      val con = JMXConnectorFactory.connect(new JMXServiceURL(ApplicationProperties.JMX_RMI_FORMAT.format(key)), null)
      val con = connectWithTimeout(new JMXServiceURL(ApplicationProperties.JMX_RMI_FORMAT.format(key)), 5000, TimeUnit.MILLISECONDS)
      val instance = new JMXConnect(key, con, server)
      _instances put(key, instance)
      info("JMX Factory Size : " + _instances.size)
      instance
    } else {
      _instances.get(key) match {
        case Some(a) => a
        case None => null
      }
    }
  }

  def getKey(server: Server): String = {
    server.host + ":" + server.port
  }

  def removeConnectionPool(server: Server) = {
    val key = getKey(server)
    _instances.remove(getKey(server))
  }

  def preQuery(query: String): (String, Boolean) = {
    val a = query.indexOf('*')
    if (a == -1) {
      (query, false)
    } else {
      (query.substring(0, a + 1), true)
    }
  }

  def close: Unit = {
    info("JMX Connection Closing start!")
    val iter = _instances.iterator
    while (iter.hasNext) {
      iter.next()._2.getConnector.close()
    }
    _instances.clear
    info("JMX Connection Closed!")
  }
}

class JMXConnect(key: String, jmxConn: JMXConnector, server: Server) extends Logging {
  def getConnector: JMXConnector = {
    jmxConn
  }

  def get: Seq[FormattedMessage[String, Double]] = {
    val metaAndDataList = server.queries.map(queryObj => {
      val mBeanList = makeQuery(mutable.Set(new ObjectName(queryObj.obj)))
      //      val metaAndData = mBeanList.filterNot(a => a.getKeyProperty("topic") == "__consumer_offsets").map(objName => {
      val metaAndData = mBeanList.map(objName => {
        val ht = objName.getKeyPropertyList
        val tags = javaHashtableToMap(ht)
        (Meta(queryObj.resultAlias, server.alias, tags), getAttributes(jmxConn.getMBeanServerConnection, objName, queryObj.attr.toArray))
      }).toList
      metaAndData
    })
    parseDataAndMeta(metaAndDataList)
  }

  def javaHashtableToMap(ht: util.Hashtable[String, String]): mutable.Map[String, String] = {
    val ks = ht.keys()
    val hm = new util.HashMap[String, String]()
    while (ks.hasMoreElements) {
      val key = ks.nextElement()
      val value = ht.get(key)
      hm.put(key, value)
    }
    hm.asScala
  }

  def makeQuery(queries: mutable.Set[ObjectName]): mutable.Set[ObjectName] = {
    val res = queries.map(query => {
      if (JMXConnectFactory.preQuery(query.toString)._2) {
        makeQuery(getMbeans(jmxConn.getMBeanServerConnection, query))
      } else {
        queries
      }
    })

    if (res.nonEmpty)
      res.reduceLeft(_ ++ _)
    else {
      mutable.Set[ObjectName]()
    }
  }

  def parseDataAndMeta(dataAndMetaList: List[List[(Meta, AttributeList)]]): List[FormattedMessage[String, Double]] = {
    dataAndMetaList.flatMap(messages => messages.flatMap(element => {
      val meta = element._1
      val elementData = element._2
      for {index <- 0 until elementData.size} yield {
        val attribute = elementData.get(index).asInstanceOf[Attribute]
        val data = attribute.getValue
        val tagMap = (meta.tags += ("attributeName" -> attribute.getName, "hostName" -> meta.hostname)).toMap
        val formatted = data match {
          case iter: CompositeDataSupport =>
            val values = iter.getCompositeType.keySet.asScala.map(key => key -> iter.get(key).toString.toDouble).toMap
            new FormattedMessage(meta.measurement, tagMap, values)
          case _ =>
            new FormattedMessage(meta.measurement, tagMap, Map(attribute.getName -> data.toString.toDouble))
        }
        formatted
      }
    }))
  }

  def getMbeans(mbsc: MBeanServerConnection, objectName: ObjectName): mutable.Set[ObjectName] = {
    try {
      mbsc.queryNames(objectName, null).asScala
    } catch {
      case e: Exception => {
        JMXConnectFactory._instances.remove(key)
        error("getQueryName Error. Query is " + objectName.toString + " , Exception : " + e)
        mutable.Set()
      }
      case e2: Throwable => {
        error("Unexpected Exception. Query is " + objectName.toString + " , Throwable : " + e2)
        JMXConnectFactory._instances.remove(key)
        throw e2
      }
    }
  }

  def getAttributes(mbsc: MBeanServerConnection, objectName: ObjectName, attrList: Array[String]): AttributeList = {
    try {
      mbsc.getAttributes(objectName, attrList)
    } catch {
      case e: Exception => {
        error("getAttributes Error Attribute is : " + objectName.toString + " , Exception : " + e)
        new AttributeList
      }
      case e2: Throwable => {
        error("Unexpected Exception. Query is " + objectName.toString + " , Throwable : " + e2)
        throw e2
      }
    }
  }
}
