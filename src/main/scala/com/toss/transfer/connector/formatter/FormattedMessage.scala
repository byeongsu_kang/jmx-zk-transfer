package com.toss.transfer.connector.formatter

/**
  * Created by 1002718 on 2017. 2. 6..
  */
class FormattedMessage[K,V](measurement: String, tags: Map[String, String], fields: Map[K, V]) {
  def getMeasurement={
    measurement
  }
  def getFields = {
    fields
  }

  def getTags = {
    tags
  }
}
